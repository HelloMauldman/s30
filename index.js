//packages
const express = require("express");
const mongoose = require("mongoose");
//packages

const app = express();

const port = 3001;

//middleware

app.use(express.json())

app.use(express.urlencoded({extended:true}));

//middleware

app.get("/", (req, res)=>{
	res.send("Hello World!")
})





mongoose.connect("mongodb+srv://admin:admin1234@zuitt-bootcamp.exnj7.mongodb.net/s30?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))

db.once("open", ()=>{console.log("We're connected to the cloud database.")})

const taskSchema = new mongoose.Schema(
	{
		name : String,
		status : {
			type:String,
			default: "pending"
		}
	}
);

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (req, res)=>{
	Task.findOne({name: req.body.name}, (err, result)=>{
		if(result !== null && result.name == req.body.name){
			return res.send("Collection already exists.")
		}
		else{
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error()
				}
				else{
					return res.status(201).send("New Task created")
				}
			})
		}
	})
})

//Get all task
//"Find is a mongoose method that is similar to mongoddb find, and an empty{} means it returns all the documents and stores them in the "result" aprameter of the callback function"
app.get("/tasks", (req, res)=>{
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}
		else{
			return res.status(200).json({data: result})
		}
	})
})


// User Collection/Model
//1. create a user schema

 const userSchema = new mongoose.Schema(
 	{
 		username: String,
 		password: String
 	}
 )


const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res)=>{
	User.findOne({username: req.body.username}, (err, result)=>{
		if(result !== null && result.username == req.body.username){
			return res.send("Collection entered already exists.")
		}
		else{
			if(req.body.username !== "" && req.body.password !== ""){
				let newUser = new User({
				username: req.body.username,
				password: req.body.password
				})
				newUser.save((saveErr, savedTask)=>{
					if(saveErr){
						return console.error()
					}
					else{
						return res.status(201).send("New User created.")
					}
				})
			}
			else{
				return res.send("Username and/or password should not be blank.")
			}
		}
	})
})


app.get("/users",(req, res)=>{
	User.find({},(err, result)=>{
		if(err){
			return console.log(err)
		}
		else{
			return res.status(200).json({data: result})
		}

	})
})


app.listen(port, ()=>{
	console.log(`server is running at port ${port}`)
})

/* connection string from mongo db
mongodb+srv://admin:admin1234@zuitt-bootcamp.exnj7.mongodb.net/s30?retryWrites=true&w=majority
*/
